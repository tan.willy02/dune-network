.. _support:

Technical Support
=================

If you need help understanding how the Dune protocol works or if you
have technical questions about the software, here are a few resources
to find answers.

- `This documentation! <https://dune.network/docs/>`_
  Make sure to go through this technical documentation before asking
  elsewhere, there is also a searchbox on the top left corner.
.. - Dune `Stack Exchange <https://dune.stackexchange.com>`_ is live
..  (still in beta). If you don't find the answers you are looking for,
..  feel free to ask questions!
- There is a sub-reddit at https://www.reddit.com/r/dune-network/ that is the
  main meeting point for the Dune community, for technical,
  economical and just random questions. They also have a nicely
  curated list of resources.
.. - For anything baking related, `Obsidian Systems
..  <https://obsidian.systems>`_ is running a dedicated Slack channel,
..  contact them to get access.
.. - There is a *#dune* IRC channel on *freenode* that is reserved for
..  technical discussions and is always very active.
.. - There is a matrix channel *Dune* that you can join `here <https://riot.im/app/#/room/#dune:matrix.org>`_.
