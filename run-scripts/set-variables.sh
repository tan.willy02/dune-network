#!/bin/sh

export COMMAND_NAME

source $SCRIPTS_DIR/default-variables.sh

if [ -f $SCRIPTS_DIR/dune-env.sh ]; then
    echo Executing $SCRIPTS_DIR/dune-env.sh
    source $SCRIPTS_DIR/dune-env.sh
else
    echo 'You can create $HOME/dune-env.sh to configure your install'
    echo Default is:
    cat $SCRIPTS_DIR/default-variables.sh
fi

DUNE_DATADIR=$DUNE_EXECDIR/server
DUNE_LOGDIR=$DUNE_EXECDIR/log
DUNE_RUNDIR=$DUNE_EXECDIR/run
DUNE_CMDDIR=$DUNE_EXECDIR/cmd
DUNE_CLIDIR=$DUNE_EXECDIR/client

if [ -z "$PROCESS_NAME" ]; then
    PROCESS_NAME=$COMMAND_NAME
fi

PIDFILE=$DUNE_RUNDIR/$PROCESS_NAME.pid
LOGFILE=$DUNE_LOGDIR/$PROCESS_NAME.log
INIFILE=$DUNE_DATADIR/version.json


export DUNE_SRCDIR
export DUNE_DATADIR
export DUNE_LOGDIR
export DUNE_RUNDIR
export DUNE_CMDDIR

export SCRIPTS_DIR

export PIDFILE
export LOGFILE
export INIFILE
