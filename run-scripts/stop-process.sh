#!/bin/sh

if test -z "$SCRIPTS_DIR"; then
    echo "Missing SCRIPTS_DIR"
    exit 2
fi

if test -z "$PIDFILE"; then
    echo "Missing PIDFILE"
    exit 2
fi

if test -z "$LOGFILE"; then
    echo "Missing LOGFILE"
    exit 2
fi

$SCRIPTS_DIR/check-status.sh
EXIT=$?

if [ $EXIT -eq 2 ]; then
    echo "Error: script error"
    exit 2
fi

if [ $EXIT -eq 0 ]; then

    PID=$(cat $PIDFILE)
    echo "$COMMAND_NAME is running (pid=$PID)..."
    kill $PID
    echo "Process received signal INTR. Waiting 5 seconds..."
    sleep 1
    if kill -0 $PID; then
	sleep 1
	if kill -0 $PID; then
	    sleep 1
	    if kill -0 $PID; then
		sleep 1
		if kill -0 $PID; then
		    sleep 1
		    if kill -0 $PID; then
			echo "$COMMAND_NAME is still running..."
			echo "Use the following command to watch it, then call this script again."
			echo tail -f $LOGFILE
			exit 2
		    else
			echo "$COMMAND_NAME is down. Removing pid file."
			rm -f $PIDFILE
		    fi
		else
		    echo "$COMMAND_NAME is down. Removing pid file."
		    rm -f $PIDFILE
		fi
	    else
		echo "$COMMAND_NAME is down. Removing pid file."
		rm -f $PIDFILE
	    fi
	else
	    echo "$COMMAND_NAME is down. Removing pid file."
	    rm -f $PIDFILE
	fi
    else
	echo "$COMMAND_NAME is down. Removing pid file."
	rm -f $PIDFILE
    fi
fi

		
