#!/bin/sh

# COMMAND_SOURCE: where is the binary to be run ?
if [ -z "$COMMAND_SOURCE" ]; then
    echo "Missing argument COMMAND_SOURCE"
    exit 2
fi

# SCRIPTS_DIR: where are these scripts ?
if [ -z "$SCRIPTS_DIR" ]; then
    echo "Missing argument SCRIPTS_DIR"
    exit 2
fi

if [ -z "$DUNE_DATADIR" ]; then
    echo "Missing argument DUNE_DATADIR"
    exit 2
fi

if [ -z "$COMMAND_NAME" ]; then
    echo "Missing argument COMMAND_NAME"
    exit 2
fi

if [ -z "$COMMAND_ARGS" ]; then
    echo "Missing argument COMMAND_ARGS"
    exit 2
fi

if [ -z "$PIDFILE" ]; then
    echo "Missing argument PIDFILE"
    exit 2
fi

if [ -z "$LOGFILE" ]; then
    echo "Missing argument LOGFILE"
    exit 2
fi

if [ -z "$DUNE_ROTATE" ]; then
    echo "Missing argument DUNE_ROTATE"
    exit 2
fi

$SCRIPTS_DIR/check-status.sh

if [ $? -ne 0 ]; then

    mkdir -p $DUNE_DATADIR
    cd $DUNE_DATADIR

    if ! [ -f "$COMMAND_SOURCE" ]; then
	echo "Missing source $COMMAND_SOURCE"
	exit 2
    fi
    
    rm -f $DUNE_DATADIR/$COMMAND_NAME
    cp -f $COMMAND_SOURCE $DUNE_DATADIR/$COMMAND_NAME

    $DUNE_ROTATE $LOGFILE
    echo $DUNE_DATADIR/$COMMAND_NAME $COMMAND_ARGS
    $DUNE_DATADIR/$COMMAND_NAME $COMMAND_ARGS >& $LOGFILE &
    echo $! > $PIDFILE
    echo "$COMMAND_NAME started."
    sleep 1
    $SCRIPTS_DIR/check-status.sh
    echo "You should run to watch the log:"
    echo tail -f $LOGFILE
fi
