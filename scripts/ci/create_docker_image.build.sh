#! /bin/sh

set -e

ci_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
script_dir="$(dirname "$ci_dir")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

. "$script_dir"/version.sh

tmp_dir=$(mktemp -dt dune-network.opam.dune-network.XXXXXXXX)

cleanup () {
    set +e
    echo Cleaning up...
    rm -rf "$tmp_dir"
    rm -rf Dockerfile
}
trap cleanup EXIT INT

image_name="${1:-dune_build}"
image_version="${2:-latest}"
base_image="${3-${image_name}_deps:${image_version}}"

mkdir -p "$tmp_dir"/dune-network/scripts
cp -a Makefile "$tmp_dir"/dune-network
cp -a Makefile-dune.* "$tmp_dir"/dune-network
cp -a active_protocol_versions "$tmp_dir"/dune-network
cp -a scripts/version.sh "$tmp_dir"/dune-network/scripts/
cp -a src "$tmp_dir"/dune-network
cp -a vendors "$tmp_dir"/dune-network

cat <<EOF > "$tmp_dir"/Dockerfile
FROM $base_image
COPY --chown=tezos:nogroup dune-network dune-network
RUN opam exec -- make -C dune-network all build-test
EOF

echo
echo "### Building dune-network..."
echo

docker build -t "$image_name:$image_version" "$tmp_dir"

echo
echo "### Successfully build docker image: $image_name:$image_version"
echo
