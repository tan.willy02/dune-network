# (* Smart contract for voting. Winners of vote split the contract
#    balance at the end of the voting period. *)
#
# (** Type of storage for this contract *)
# type storage = {
#   voters : (address, unit) big_map; (** Used to register voters *)
#   votes : (string, nat) map; (** Keep track of vote counts *)
#   addresses : (string, key_hash) map; (** Addresses for payout *)
# }
#
# (** Initial storage *)
# let%init storage addresses = {
#   (* Initialize vote counts to zero *)
#   votes = Map.fold (fun ((name, _kh), votes) ->
#       Map.add name 0p votes
#     ) addresses Map;
#   addresses;
#   voters = BigMap ; (* No voters *)
# }
#
# (** Entry point for voting.
#     @param choice A string corresponding to the candidate *)
# let%entry vote choice storage = begin [@fee
#   let fee = 0.01_DUN in
#   let max_storage = 5p (* bytes *) in
#   fee, max_storage
# ]
#   (* Voter must send at least 5DUN to vote *)
#   if Current.amount () < 5.00DUN then
#     failwith "Not enough money, at least 5DUN to vote";
#   (* Voter cannot vote twice *)
#   if Map.mem (Current.sender ()) storage.voters then
#     failwith ("Has already voted", Current.sender ());
#   let votes = storage.votes in
#   match Map.find choice votes with
#   | None ->
#     (* Vote must be for an existing candidate *)
#     failwith ("Bad vote", choice)
#   | Some x ->
#     (* Increase vote count for candidate *)
#     let storage = storage.votes <- Map.add choice (x + 1p) votes in
#     (* Register voter *)
#     let storage =
#       storage.voters <- Map.add (Current.sender ()) () storage.voters in
#     (* Return updated storage *)
#     ([], storage)
# end
#
# (* Auxiliary function : returns the list of candidates with the
#    maximum number of votes (there can be more than one in case of
#    draw). *)
# let find_winners votes =
#   let winners, _max =
#     Map.fold (fun ((name, nb), (winners, max)) ->
#         if nb = max then
#           name :: winners, max
#         else if nb > max then
#           [name], nb
#         else winners, max
#       ) votes ([], 0p) in
#   winners
#
# (** Entry point for paying winning candidates. *)
# let%entry payout () storage = begin [@fee
#   let fee =
#     0.008_DUN + Map.size storage.votes * 0.000_25_DUN in
#   (fee, 0p (* no burn*))
# ]
#   (* Indentify winners of vote *)
#   let winners = find_winners storage.votes in
#   (* Balance of contract is split equally between winners *)
#   let amount = match Current.balance () / List.length winners with
#     | None -> failwith "No winners"
#     | Some (v, _rem) -> v in
#   (* Generate transfer operations *)
#   let operations = List.map (fun name ->
#       let dest = match Map.find name storage.addresses with
#         | None -> failwith () (* This cannot happen *)
#         | Some d -> d in
#       Account.transfer ~amount ~dest
#     ) winners in
#   (* Return list of operations. Storage is unchanged *)
#   operations, storage
# end
parameter (or :_entries (string %vote) (unit %payout));
storage
  (pair :storage
     (big_map :voters address unit)
     (pair (map %votes string nat) (map %addresses string key_hash)));
code { DUP ;
       DIP { CDR @storage_slash_1 } ;
       CAR @parameter_slash_2 ;
       DUP @parameter ;
       IF_LEFT
         { RENAME @choice_slash_13 ;
           DUUUP @storage ;
           PUSH mutez 5000000 ;
           AMOUNT ;
           COMPARE ;
           LT ;
           IF { PUSH string "Not enough money, at least 5DUN to vote" ; FAILWITH }
              { UNIT } ;
           DROP ;
           DUP @storage ;
           CAR %voters ;
           SENDER ;
           MEM ;
           IF { SENDER ; PUSH string "Has already voted" ; PAIR ; FAILWITH } { UNIT } ;
           DROP ;
           DUP @storage ;
           CDAR @votes %votes ;
           DUP @votes ;
           DUUUUP @choice ;
           GET ;
           IF_NONE
             { DUUUP @choice ; PUSH string "Bad vote" ; PAIR ; FAILWITH }
             { DUUUP @storage ;
               DUP ;
               CAR %voters ;
               SWAP ;
               CDR ;
               CDR %addresses ;
               DUUUUP @votes ;
               PUSH nat 1 ;
               DUUUUUP @x ;
               ADD ;
               DUUUUUUUUP @choice ;
               DIP { SOME } ;
               UPDATE ;
               PAIR %votes %addresses ;
               SWAP ;
               PAIR @storage %voters ;
               DUP @storage ;
               CDR ;
               DUUP @storage ;
               CAR %voters ;
               UNIT ;
               SENDER ;
               DIP { SOME } ;
               DIIIIP { DROP ; DROP } ;
               UPDATE ;
               PAIR @storage %voters ;
               NIL operation ;
               PAIR } ;
           DIP { DROP ; DROP ; DROP } }
         { RENAME @__slash_19 ;
           DUUUP @storage ;
           DUP @storage ;
           CDAR %votes ;
           PUSH (pair (list string) nat) (Pair {} 0) ;
           DUUP @votes ;
           ITER { RENAME @_name_nb_winners_max_slash_4 ;
                  DIP { DUP } ;
                  PAIR ;
                  DUP ;
                  CAR ;
                  CAR @name ;
                  DUUP ;
                  CAR ;
                  CDR @nb ;
                  DUUUP ;
                  CDR ;
                  CAR @winners ;
                  DUUUUP ;
                  CDR ;
                  CDR @max ;
                  DUP @max ;
                  DUUUUP @nb ;
                  COMPARE ;
                  EQ ;
                  IF { DUP @max ; NIL string ; DUUUUUUP @name ; CONS ; PAIR }
                     { DUP @max ;
                       DUUUUP @nb ;
                       COMPARE ;
                       GT ;
                       IF { DUUUP @nb ; NIL string ; DUUUUUUP @name ; CONS ; PAIR }
                          { DUP @max ; DUUUP @winners ; PAIR } } ;
                  DIP { DROP ; DROP ; DROP ; DROP ; DROP ; DROP } } ;
           DIP { DROP } ;
           RENAME @_winners__max ;
           CAR @winners ;
           DUUP @storage ;
           DUUP @winners ;
           MAP { RENAME @name_slash_26 ;
                 DUUUUP @storage ;
                 CDDR %addresses ;
                 DUUP @name ;
                 GET ;
                 IF_NONE { UNIT ; FAILWITH } {} ;
                 RENAME @dest ;
                 IMPLICIT_ACCOUNT ;
                 DUUUUP @winners ;
                 SIZE ;
                 BALANCE ;
                 EDIV ;
                 IF_NONE { PUSH string "No winners" ; FAILWITH } { CAR @v } ;
                 DIIP { DROP } ;
                 RENAME @amount ;
                 UNIT ;
                 TRANSFER_TOKENS } ;
           DIIP { DROP ; DROP ; DROP } ;
           RENAME @operations ;
           PAIR } ;
       DIP { DROP ; DROP } };
code @fee
  { DUP ;
    DIP { CDR @storage_slash_1 } ;
    CAR @parameter_slash_2 ;
    DUP @parameter ;
    IF_LEFT
      { RENAME @choice_slash_40 ;
        PUSH @max_storage nat 5 ;
        PUSH @fee mutez 10000 ;
        PAIR ;
        DIP { DROP } }
      { RENAME @__slash_44 ;
        PUSH nat 0 ;
        PUSH mutez 250 ;
        DUUUUUP @storage ;
        CDAR %votes ;
        SIZE ;
        MUL ;
        PUSH mutez 8000 ;
        ADD @fee ;
        PAIR ;
        DIP { DROP } } ;
    DIP { DROP ; DROP } };
