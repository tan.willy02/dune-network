
type node = {
  state : State.t ;
  p2p: Distributed_db.p2p ;
}

module Encoding = struct
  open Data_encoding

  let version =
    RPC_service.get_service
      ~description:"Return the current version."
      ~query: RPC_query.empty
      ~output:
        (obj4
           (req "commit_date" string)
           (req "build_date" string)
           (req "commit" string)
           (req "branch" string))
      RPC_path.(root / "dune" / "version")

  let banned_points =
    RPC_service.get_service
      ~description:"Return the list of banned points."
      ~query: RPC_query.empty
      ~output:
        (list
           (obj2
              (req "id" P2p_point.Id.encoding)
              (req "info" P2p_point.Info.encoding)))
      RPC_path.(root / "dune" / "banned" / "points")

  let banned_peers =
    RPC_service.get_service
      ~description:"Return the list of banned peers."
      ~query: RPC_query.empty
      ~output:
        (list
           (obj2
              (req "id" P2p_peer.Id.encoding)
              (req "info"
                 (P2p_peer.Info.encoding Peer_metadata.encoding
                    Connection_metadata.encoding))))
      RPC_path.(root / "dune" / "banned" / "peers")

end

module Handler = struct

  let version _node () () =
    return (Tezos_base.Current_git_info.committer_date,
            Tezos_base.Current_git_info.build_date,
            Tezos_base.Current_git_info.commit_hash,
            Tezos_base.Current_git_info.branch)


  let banned_points node () () =
    return (P2p_directory.banned_points node.p2p)

  let banned_peers node () () =
    return (P2p_directory.banned_peers node.p2p)

end

let build_rpc_directory ( node : node  ) =

  let dir : unit RPC_directory.t ref = ref RPC_directory.empty in

  let register0 s f =
    dir := RPC_directory.register !dir s (fun () p q -> f p q) in
  (*
  let register1 s f =
    dir := RPC_directory.register !dir s (fun ((), a) p q -> f a p q) in
  let register2 s f =
    dir := RPC_directory.register !dir s (fun (((), a), b) p q -> f a b p q) in
*)

  register0 Encoding.version (Handler.version node);
  register0 Encoding.banned_points (Handler.banned_points node);
  register0 Encoding.banned_peers (Handler.banned_peers node);
  !dir
