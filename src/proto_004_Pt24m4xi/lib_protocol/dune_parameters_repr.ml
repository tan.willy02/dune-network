(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* This module defines a set of parameters that can be defined in the
   manage_protocol operation. We could not use Parameters_repr,
   because uninitialized fields in Parameters_repr default to a
   constant, instead of the current value. Instead, here, the default
   is always None, so that [patch_constant] will have the expected
   behavior.
*)


module TYPES = struct

  type ('tez_repr, 'period_repr) _parameters = {
    (* fixed *)
    proof_of_work_nonce_size : int option ;
    nonce_length : int option ;
    max_revelations_per_block : int option ;
    max_operation_data_length : int option;
    max_proposals_per_delegate : int option;

    (* parametric *)
    preserved_cycles: int option;
    blocks_per_cycle: int32 option;
    blocks_per_commitment: int32 option;
    blocks_per_roll_snapshot: int32 option;
    blocks_per_voting_period: int32 option;
    time_between_blocks: 'period_repr list option;
    endorsers_per_block: int option;
    hard_gas_limit_per_operation: Z.t option;
    hard_gas_limit_per_block: Z.t option;
    proof_of_work_threshold: int64 option;
    tokens_per_roll: 'tez_repr option;
    michelson_maximum_type_size: int option;
    seed_nonce_revelation_tip: Tez_repr.t option;
    origination_size: int option;
    block_security_deposit: Tez_repr.t option;
    endorsement_security_deposit: Tez_repr.t option;
    block_reward: Tez_repr.t option;
    endorsement_reward: Tez_repr.t option;
    cost_per_byte: Tez_repr.t option;
    hard_storage_limit_per_operation: Z.t option;
    hard_gas_limit_to_pay_fees: Z.t option;
    max_operation_ttl : int option;

    (* Semantic protocol revisions *)
    protocol_revision: int option;
  }

  type 'commitment_repr _accounts  = {
    commitments : 'commitment_repr list ;
    change_foundation_pubkey : Signature.Public_key.t option ;
    change_foundation_bakers :
      ( Signature.Public_key_hash.t * bool ) list ;
  }

end

include TYPES

type parameters = (Tez_repr.t, Period_repr.t) _parameters
type accounts = Commitment_repr.t _accounts

open Data_encoding

let parameters_encoding =
  conv
    (fun { proof_of_work_nonce_size  ;
           nonce_length  ;
           max_revelations_per_block  ;
           max_operation_data_length ;
           max_proposals_per_delegate ;
           preserved_cycles ;
           blocks_per_cycle ;
           blocks_per_commitment ;
           blocks_per_roll_snapshot ;
           blocks_per_voting_period ;
           time_between_blocks ;
           endorsers_per_block ;
           hard_gas_limit_per_operation ;
           hard_gas_limit_per_block ;
           proof_of_work_threshold ;
           tokens_per_roll ;
           michelson_maximum_type_size ;
           seed_nonce_revelation_tip ;
           origination_size ;
           block_security_deposit ;
           endorsement_security_deposit ;
           block_reward ;
           endorsement_reward ;
           cost_per_byte ;
           hard_storage_limit_per_operation ;
           hard_gas_limit_to_pay_fees ;
           protocol_revision ;
           max_operation_ttl ;
         } ->
      ( proof_of_work_nonce_size  ,
        nonce_length  ,
        max_revelations_per_block  ,
        max_operation_data_length ,
        max_proposals_per_delegate ,
        preserved_cycles ,
        blocks_per_cycle ,
        blocks_per_commitment ,
        blocks_per_roll_snapshot ,
        blocks_per_voting_period ,
        time_between_blocks ,
        endorsers_per_block ,
        hard_gas_limit_per_operation ,
        hard_gas_limit_per_block ,
        proof_of_work_threshold ,
        tokens_per_roll ,
        michelson_maximum_type_size ,
        seed_nonce_revelation_tip ,
        origination_size ,
        block_security_deposit ,
        endorsement_security_deposit ,
        block_reward ,
        endorsement_reward ,
        cost_per_byte ,
        hard_storage_limit_per_operation ,
        hard_gas_limit_to_pay_fees ,
        protocol_revision ,
        max_operation_ttl
      )
    )
    (fun  ( proof_of_work_nonce_size  ,
            nonce_length  ,
            max_revelations_per_block  ,
            max_operation_data_length ,
            max_proposals_per_delegate ,
            preserved_cycles ,
            blocks_per_cycle ,
            blocks_per_commitment ,
            blocks_per_roll_snapshot ,
            blocks_per_voting_period ,
            time_between_blocks ,
            endorsers_per_block ,
            hard_gas_limit_per_operation ,
            hard_gas_limit_per_block ,
            proof_of_work_threshold ,
            tokens_per_roll ,
            michelson_maximum_type_size ,
            seed_nonce_revelation_tip ,
            origination_size ,
            block_security_deposit ,
            endorsement_security_deposit ,
            block_reward ,
            endorsement_reward ,
            cost_per_byte ,
            hard_storage_limit_per_operation ,
            hard_gas_limit_to_pay_fees ,
            protocol_revision ,
            max_operation_ttl
          ) ->
      { proof_of_work_nonce_size  ;
        nonce_length  ;
        max_revelations_per_block  ;
        max_operation_data_length ;
        max_proposals_per_delegate ;
        preserved_cycles ;
        blocks_per_cycle ;
        blocks_per_commitment ;
        blocks_per_roll_snapshot ;
        blocks_per_voting_period ;
        time_between_blocks ;
        endorsers_per_block ;
        hard_gas_limit_per_operation ;
        hard_gas_limit_per_block ;
        proof_of_work_threshold ;
        tokens_per_roll ;
        michelson_maximum_type_size ;
        seed_nonce_revelation_tip ;
        origination_size ;
        block_security_deposit ;
        endorsement_security_deposit ;
        block_reward ;
        endorsement_reward ;
        cost_per_byte ;
        hard_storage_limit_per_operation ;
        hard_gas_limit_to_pay_fees ;
        protocol_revision ;
        max_operation_ttl ;
      })
    (obj28
       (opt "proof_of_work_nonce_size" uint8)
       (opt "nonce_length" uint8)
       (opt "max_revelations_per_block"  uint8 )
       (opt "max_operation_data_length"  int31 )
       (opt "max_proposals_per_delegate"  uint8 )
       (opt "preserved_cycles" uint8 )
       (opt "blocks_per_cycle" int32 )
       (opt "blocks_per_commitment" int32 )
       (opt "blocks_per_roll_snapshot" int32 )
       (opt "blocks_per_voting_period" int32 )
       (opt "time_between_blocks" (list Period_repr.encoding) )
       (opt "endorsers_per_block" uint16 )
       (opt "hard_gas_limit_per_operation" z )
       (opt "hard_gas_limit_per_block" z )
       (opt "proof_of_work_threshold" int64 )
       (opt "tokens_per_roll" Tez_repr.encoding )
       (opt "michelson_maximum_type_size" uint16 )
       (opt "seed_nonce_revelation_tip" Tez_repr.encoding )
       (opt "origination_size" int31 )
       (opt "block_security_deposit" Tez_repr.encoding )
       (opt "endorsement_security_deposit" Tez_repr.encoding )
       (opt "block_reward" Tez_repr.encoding )
       (opt "endorsement_reward" Tez_repr.encoding )
       (opt "cost_per_byte" Tez_repr.encoding )
       (opt "hard_storage_limit_per_operation" z )
       (opt "hard_gas_limit_to_pay_fees" z )
       (opt "protocol_revision" int31)
       (opt "max_operation_ttl" int31)
    )

let accounts_encoding =
  conv
    (fun
      { commitments ;
        change_foundation_pubkey ;
        change_foundation_bakers } ->
      ( commitments,
        change_foundation_pubkey ,
        change_foundation_bakers ))
    (fun
      ( commitments,
        change_foundation_pubkey,
        change_foundation_bakers ) ->
      { commitments ;
        change_foundation_pubkey ;
        change_foundation_bakers })
    (obj3
       (dft "commitments"
          (list Commitment_repr.encoding) [])
       (opt "change_foundation_pubkey"
          Signature.Public_key.encoding)
       (dft "change_foundation_bakers"
          (list
             (tup2 Signature.Public_key_hash.encoding bool) ) [] )
    )

let ifsome option dft =
  match option with
  | None -> dft
  | Some v -> v

let patch_constants p cst =
  let module C = Constants_repr in
  {
    C.preserved_cycles = ifsome p.preserved_cycles cst.C.preserved_cycles ;
    C.blocks_per_cycle = ifsome p.blocks_per_cycle cst.C.blocks_per_cycle ;
    C.blocks_per_commitment = ifsome p.blocks_per_commitment cst.C.blocks_per_commitment ;
    C.blocks_per_roll_snapshot = ifsome p.blocks_per_roll_snapshot cst.C.blocks_per_roll_snapshot ;
    C.blocks_per_voting_period = ifsome p.blocks_per_voting_period cst.C.blocks_per_voting_period ;
    C.time_between_blocks = ifsome p.time_between_blocks cst.C.time_between_blocks ;
    C.endorsers_per_block = ifsome p.endorsers_per_block cst.C.endorsers_per_block ;
    C.hard_gas_limit_per_operation = ifsome p.hard_gas_limit_per_operation cst.C.hard_gas_limit_per_operation ;
    C.hard_gas_limit_per_block = ifsome p.hard_gas_limit_per_block cst.C.hard_gas_limit_per_block ;
    C.proof_of_work_threshold = ifsome p.proof_of_work_threshold cst.C.proof_of_work_threshold ;
    C.tokens_per_roll = ifsome p.tokens_per_roll cst.C.tokens_per_roll ;
    C.michelson_maximum_type_size = ifsome p.michelson_maximum_type_size cst.C.michelson_maximum_type_size ;
    C.seed_nonce_revelation_tip = ifsome p.seed_nonce_revelation_tip cst.C.seed_nonce_revelation_tip ;
    C.origination_size = ifsome p.origination_size cst.C.origination_size ;
    C.block_security_deposit = ifsome p.block_security_deposit cst.C.block_security_deposit ;
    C.endorsement_security_deposit = ifsome p.endorsement_security_deposit cst.C.endorsement_security_deposit ;
    C.block_reward = ifsome p.block_reward cst.C.block_reward ;
    C.endorsement_reward = ifsome p.endorsement_reward cst.C.endorsement_reward ;
    C.cost_per_byte = ifsome p.cost_per_byte cst.C.cost_per_byte ;
    C.hard_storage_limit_per_operation = ifsome p.hard_storage_limit_per_operation cst.C.hard_storage_limit_per_operation;

    C.test_chain_duration = cst.C.test_chain_duration ;
    C.hard_gas_limit_to_pay_fees = ifsome p.hard_gas_limit_to_pay_fees cst.C.hard_gas_limit_to_pay_fees ;
    C.max_operation_ttl = ifsome p.max_operation_ttl cst.C.max_operation_ttl;
  }


(* Signature re-exported by Alpha_context, to avoid multiple
   redefinitions of types and values *)

module type S = sig

  type tez_repr
  type period_repr
  type commitment_repr
  type parametric

  type parameters =
    (tez_repr, period_repr) TYPES._parameters
  type accounts =
    commitment_repr TYPES._accounts

  val parameters_encoding : parameters Data_encoding.t
  val accounts_encoding : accounts Data_encoding.t
  val patch_constants : parameters -> parametric -> parametric
end
